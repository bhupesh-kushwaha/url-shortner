<div class="card">
    <div class="card-body">
        @if(isset($response) && !empty($response))
            @if(!empty($response['full_url']))
                <p>Short Url :
                    <a target="_blank" href="{{ $response['full_url'][2]['short_url'] }}">
                        {{ $response['full_url'][2]['short_url'] }}
                    </a>
                </p>
            @elseif(!empty($response['short_url']))
                @if($response['short_url'][0] === false)
                    <p>Nothing to find!
                @else
                    <p>Actual Url :
                        <a target="_blank" href="{{ $response['short_url'][2]['full_url'] }}">
                            {{ $response['short_url'][2]['full_url'] }}
                        </a>
                    </p>
                @endif
            @endif
        @else
        <div class="alert alert-warning">
            Nothing to find!
        </div>
        @endif

    </div>
</div>

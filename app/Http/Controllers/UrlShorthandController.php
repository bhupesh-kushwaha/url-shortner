<?php

namespace App\Http\Controllers;

use App\UrlShorthand;

class UrlShorthandController extends Controller
{
    /**
     * This is for get home page for browser
     * Check the option parameter value and do action on it
     * Redirect self domain or third party url
     *
     * @param null $short
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($short = null)
    {
        if(!empty($short)){
            $getData = UrlShorthand::where("short_url", 'like', "%$short%")->first();

            if( is_null($getData) || empty($getData->full_url)){
                return redirect()->route('home');
            }

            return redirect()->away($getData->full_url);
        }

        return view('UrlShorthand.index');
    }
}

function callAjax(method, url, param, setResponse) {
    $.ajax({
        type: method,
        url: url,
        data: param,
        success: function (response) {
            setResponse.html(response.data);
        },
        error: function (jqXhr, json, errorThrown) {
            var errors = jqXhr.responseJSON;

            var errorsHtml = generetaErrorLists(errors['error']);

            if (errorsHtml.length !== 0) {
                $("#url_error").html(errorsHtml);
            }
        }
    });
}

function generetaErrorLists(error) {
    var errors = "";
    $.each(error, function (index, value) {
        errors += '<p class="text-danger">' + value + '</p>';
    });

    return errors;
}

function validationMessage(type, action) {
    var text = "";
    if (type === 'URL') {
        if (action === "blank") {
            text = "The field must be required."
        }

        if (action === "url") {
            text = "The field must be a valid URL."
        }
    }

    return text;
}

function validation() {
    var errorAttribute = $("#url_error");
    var urlValidator = $('input[name=url]').val();

    errorAttribute.html('<p class="text-danger"></p>');

    var url_reg_expresion = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

    if (urlValidator.length === 0) {
        errorAttribute.children('p.text-danger').text(validationMessage('URL', 'url'));
        return false;
    }

    if (url_reg_expresion.test(urlValidator) === false) {
        errorAttribute.children('p.text-danger').text(validationMessage('URL', 'url'));
        return false;
    }

    return true;
}

$(document).ready(function () {
    $(document).on('keypress', '.valid', function (e) {
        if ($(this).next().length !== 0) {
            $(".invalid-error").html('');
        }
    });

    $(".btn-submit").click(function (e) {
        e.preventDefault();
        var actionUrl = $("form").data('url');
        var url = $("input[name=url]").val();
        var block_section = $('#block_section');

        var checkValid = validation();

        if (checkValid === false) {
            return false;
        }

        var param = {
            url: url
        };

        callAjax('POST', actionUrl, param, block_section)
    });
});


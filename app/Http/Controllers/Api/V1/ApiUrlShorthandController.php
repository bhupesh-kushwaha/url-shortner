<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\UrlShorthandHelper;
use App\Http\Controllers\Controller;
use App\Services\UrlShorthandService;
use Illuminate\Http\Request;

class ApiUrlShorthandController extends Controller
{
    use UrlShorthandHelper;

    /**
     * @var UrlShorthandService
     */
    protected $url_shorthand;

    /**
     * ApiUrlShorthandController constructor.
     *
     * @param UrlShorthandService $url_shorthand
     */
    public function __construct(UrlShorthandService $url_shorthand)
    {
        $this->url_shorthand = $url_shorthand;
    }

    /**
     * Get request and action for appropriate response
     * Check validation and do action if fail
     * Processing on given parameter and render blade page
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function action(Request $request)
    {
        $validator = $this->url_shorthand->shorthandValidator($request);

        if ($validator->fails()) {
            return $this->generateResponse(false, 'Invalid data.', 422, [], $validator->errors()->all());
        }

        $response = $this->url_shorthand->getUrlDoAction($request->input('url'));

        $html = view('UrlShorthand.partial.get-block', compact('response'))->render();

        return $this->generateResponse(true, "success", 200, $html);
    }
}

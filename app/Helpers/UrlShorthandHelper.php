<?php

namespace App\Helpers;

use Illuminate\Support\Str;

trait UrlShorthandHelper
{
    /**
     * Generate a json response with static key format
     *
     * @param bool $status
     * @param null $message
     * @param int $statusCode
     * @param array $data
     * @param array $error
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateResponse($status = false, $message = NULL, $statusCode = 200, $data = array(), $error = array())
    {
        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $data,
            "error" => $error
        ], $statusCode);
    }

    /**
     * Generate random key with only six character
     *
     * @return string
     */
    public function generateShortUrl()
    {
        return Str::random(6);
    }
}


<div class="alert alert-info">
    <strong>Note!</strong> When a long URL is typed in a text box it is shortened or shortened URL typed is give a full url. And when a short URL is typed in the browser, the user is
    redirected to the corresponding long URL.
</div>

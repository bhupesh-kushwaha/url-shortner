## URL Shortner – Laravel 5 | MySQL

A URL shortner service is that takes a long URL and generates a shorter version of it. When the short URL is typed in the
browser window, it redirects to the actual URL.

### TECHNOLOGY

* **Laravel**: Laravel 5.6 – PHP 7.2 or higher.
* **Twitter Bootstrap**: V4.1.1 use for css styling and js.
* **MySQL**: Database.
* **jQuery/**: Front end framework.

### Installation and Configuration

**1. Enter git clone and the repository URL at your command line::** 
~~~
git clone https://bhupesh19921@bitbucket.org/bhupesh-kushwaha/url-shortner.git
~~~

**2. Goto url-shortner directory and composer update:** 
~~~
composer update
~~~

**3. Copy `env.example` to `.env` and generate app key:** 
~~~
cp .env.example .env

php artisan key:generate
~~~

**3.1. You need to set your `APP_NAME` and `APP_URL` from `.env` file** 

**4. Create a database `url_shortener_servic` or if you want to change database name just go in .env file and change value for `DB_DATABASE` key:**

**5. Now, Run migration to create a table in your database:** 
~~~
php artisan migration
~~~

**6. Finally, Start your server:**
~~~
php artisan serve
~~~

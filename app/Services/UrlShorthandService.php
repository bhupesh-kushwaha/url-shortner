<?php

namespace App\Services;

use App\Helpers\UrlShorthandHelper;
use App\UrlShorthand;
use Validator;

class UrlShorthandService implements UrlShorthandServiceInterface
{
    use UrlShorthandHelper;

    const SHORT_URL = "short_url";

    const FULL_URL = "full_url";

    /**
     * Add validation to given request data in parameter
     *
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function shorthandValidator($request)
    {
        $rules = array(
            'url' => 'required|url'
        );

        $message = array(
            'url.required' => 'The field must be required.',
            'url.url' => 'The field must be a valid URL.',
        );

        return validator($request->all(), $rules, $message);
    }

    /**
     * To get URL action response data saved and return response
     * Also check same domain URL and return response
     *
     * @param $url
     * @return array
     */
    public function getUrlDoAction($url)
    {
        $checkDomain = $this->checkDomain($url);

        if ($checkDomain) {
            $shortUrl = UrlShorthand::where($this::SHORT_URL, $url)->first();

            if (is_null($shortUrl)) {
                return array($this::SHORT_URL => array(false, 'Failed to get shorthand url.', []));
            }

            return array($this::SHORT_URL => array(true, 'Get full url successfully.', $shortUrl->toArray()));
        }

        $fullUrl = UrlShorthand::where($this::FULL_URL, $url)->first();

        if (is_null($fullUrl)) {
            $fullUrl = $this->save($url);
        }

        return array($this::FULL_URL => array(true, 'Get shorthand url successfully.', $fullUrl->toArray()));
    }

    /**
     * Newly added data save in database
     *
     * @param $url
     * @return mixed
     */
    public function save($url)
    {
        $shortUrl = $this->checkAndGenerateShortUrl();

        return UrlShorthand::create([
            "full_url" => $url,
            "short_url" => $shortUrl
        ]);
    }

    /**
     * Check newly created key is exists or not in database
     *
     * @return mixed
     */
    public function checkAndGenerateShortUrl()
    {
        $shortUrl = $this->generateShortUrl();

        $findShortUrl = UrlShorthand::where($this::SHORT_URL, 'like', "%$shortUrl%")->first();

        if (is_null($findShortUrl)) {
            return \URL::to('/' . $shortUrl);
        }

        $this->checkAndGenerateShortUrl();
    }

    /**
     * Create a find functionality for various field and value
     *
     * @param $url
     * @param $field
     * @return mixed
     */
    public function find($url, $field)
    {
        return UrlShorthand::where($field, $url)->first();
    }

    /**
     * This function is check URL domain
     *
     * @param $url
     * @return bool
     */
    public function checkDomain($url)
    {
        return strpos($url, \URL::to('/')) !== false ? true : false;
    }
}


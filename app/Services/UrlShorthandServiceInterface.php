<?php

namespace App\Services;


interface UrlShorthandServiceInterface
{
    /**
     * Add validation to given request data in parameter
     *
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function shorthandValidator($request);

    /**
     * To get URL action response data saved and return response
     * Also check same domain URL and return response
     *
     * @param $url
     * @return array
     */
    public function getUrlDoAction($url);

    /**
     * Newly added data save in database
     *
     * @param $url
     * @return mixed
     */
    public function save($url);

    /**
     * Check newly created key is exists or not in database
     *
     * @return mixed
     */
    public function checkAndGenerateShortUrl();

    /**
     * Create a find functionality for various field and value
     *
     * @param $url
     * @param $field
     * @return mixed
     */
    public function find($url, $field);

    /**
     * This function is check URL domain
     *
     * @param $url
     * @return mixed
     */
    public function checkDomain($url);
}


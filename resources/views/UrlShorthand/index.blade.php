<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
          content="A URL shortner service is that takes a long URL and generates a shorter version of it. When the short URL is typed in the browser window, it redirects to the actual URL.">
    <meta name="author" content="Bhupesh Kushwaha">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>URL Shortner – Laravel 5 | MySQL</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css">

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>

<div class="container" style="margin-top: 1em;">
    <form data-url="{{ url('api/v1/shorthand') }}">
        <div class="row">
            <div class="col-md-12">
                <div class="card url-shorthand-card pl-3 pr-3">
                    <div class="card-body">
                        <img id="img_sex" class="url-shorthand-img"
                             src="{{ asset('images/domain.svg') }}">

                        <h2 class="card-title mb-5">URL Shortner – Laravel 5 | MySQL</h2>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <input id="url" name="url" type="url" class="form-control valid" placeholder="Enter url here">
                                <div id="url_error" class="invalid-error">

                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <button type="submit" class="form-control btn btn-primary btn-md btn-submit">Get URL</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3" style="padding=0.5em;" id="block_section"></div>

            <div class="col-md-12" style="padding=0.5em;">
                @include('UrlShorthand.partial.block-note')
            </div>

        </div>

    </form>
</div>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
</body>
</html>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlShorthand extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_url', 'short_url'
    ];
}
